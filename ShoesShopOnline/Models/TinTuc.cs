﻿namespace ShoesShopOnline.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TinTuc")]
    public partial class TinTuc
    {
        [Key]
        [Required]
        [DisplayName("Mã tin")]
        public int MaTin { get; set; }

        [Required(ErrorMessage = "Tên tin không được để trống !")]
        [StringLength(100)]
        [DisplayName("Tên tin")]
        public string TenTin { get; set; }

        [DisplayName("Ngày đăng")]
        public DateTime NgayDang { get; set; }

        [DisplayName("Mã tài khoản")]
        public int? MaTK { get; set; }

        [Required(ErrorMessage = "Nội dung không được để trống !")]
        [Column(TypeName = "ntext")]
        [DisplayName("Nội dung")]
        public string NoiDung { get; set; }

        public virtual TaiKhoanQuanTri TaiKhoanQuanTri { get; set; }
    }
}
