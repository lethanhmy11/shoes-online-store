﻿namespace ShoesShopOnline.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("SanPham")]
    public partial class SanPham
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SanPham()
        {
            AnhMoTas = new HashSet<AnhMoTa>();
        }

        [Key]
        [StringLength(10)]
        [DisplayName("Mã sản phẩm")]
        [Required(ErrorMessage = "Mã sản phẩm không được để trống !")]
        public string MaSP { get; set; }

        [StringLength(200)]
        [DisplayName("Tên sản phẩm")]
        [Required(ErrorMessage = "Tên sản phẩm không được để trống !")]
        public string TenSP { get; set; }

        [StringLength(20)]
        [DisplayName("Mã danh mục")]
        [Required(ErrorMessage = "Mã danh mục không được để trống !")]
        public string MaDM { get; set; }

        [StringLength(500)]
        [DisplayName("Mô tả")]
        [Required(ErrorMessage = "Mô tả không được để trống !")]
        public string MoTa { get; set; }

        [Column(TypeName = "money")]
        [DisplayName("Giá bán")]
        [Required(ErrorMessage = "Giá bán không được để trống !")]
        public decimal GiaBan { get; set; }

        public DateTime NgayTao { get; set; }

        public DateTime NgaySua { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AnhMoTa> AnhMoTas { get; set; }

        public virtual DanhMucSP DanhMucSP { get; set; }
    }
}
